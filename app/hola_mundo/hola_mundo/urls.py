from django.contrib import admin
from django.urls import path
from mensaje import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('login/', views.login),
    path('hola/', views.mensaje),
    path('hola2/', views.mensaje2),
    path('calculadora/', views.calculadora),
    path('parametro/<int:id>', views.parametro),
    path('cadena/<str:nombre>', views.cadena),
    path('id-cadena/<int:id>/<str:nombre>', views.id_cadena),
]
