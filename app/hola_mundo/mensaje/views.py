from django.shortcuts import render

def mensaje(request):
    calificaciones = [
        {'materia':'SO Linux','calificacion':10},
        {'materia':'Framewroks','calificacion':11},
        {'materia':'Testing','calificacion':8},
        {'materia':'Deployement','calificacion':9},
        {'materia':'Ética','calificacion':7},
        {'materia':'TSP','calificacion':10},
    ]
    context = {
        'nombre' : 'Alex',
        'semestre' : 6,
        'materias' : [
            'SO Linux',
            'Frameworks',
            'Testing',
            'Deployment',
            'Ética',
            'TSP,'
        ],
        'calificaciones': calificaciones
    }
    return render(request, 'mensaje.html', context)

def mensaje2(request):
    
    return render(request, 'hola.html')


def parametro(request, id):
    print (request)
    print (id)
    
    context = {
        'id':id
    }
    return render(request, 'hola.html', context)
    
def cadena(request, nombre):
    context = {
        'nombre':nombre
    }
    return render(request, 'hola.html', context)
    
def id_cadena(request, id, nombre):
    context = {
        'id': id,
        'nombre':nombre
    }
    return render(request, 'hola.html', context)

def login(request):
    if request.method == 'POST':
        context = {}
        username = request.POST.get('username',None)
        password = request.POST.get('password',None)
        context['username'] = username
        context['valido'] = valida_usuario(username, password)
        return render(request, 'formulario.html',context)
        
    return render(request, 'formulario.html')

def valida_usuario(username, password):
    if username == 'admin' and password == 'admin123':
        return True
    return False

def calculadora(request):
    print (request.POST)
    resultado = '';
    if request.method == 'POST':
        num1 = int(request.POST.get('num1',0))
        num2 = int(request.POST.get('num2',0))
        operacion = request.POST.get('operacion',0)
        if operacion == '+':
            resultado = num1 + num2
        if operacion == '-':
            resultado = num1 - num2
        if operacion == 'X':
            resultado = num1 * num2
        if operacion == '/':
            resultado = num1 / num2 
    
    return render(request,'calculadora.html', {'resultado':resultado})