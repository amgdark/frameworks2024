from django.db import models

# CRUD

# READ 
class UnidadAcademica(models.Model):
    nombre = models.CharField(max_length=150)
    descripcion = models.TextField('Descripción')

    def __str__(self):
        return self.nombre

class ProgramaAcademico(models.Model):
    nombre = models.CharField(max_length=150)
    descripcion = models.TextField('Descripción')
    unidad_academica = models.ForeignKey("programa_academico.UnidadAcademica",
            verbose_name='Unidad Académica', on_delete=models.DO_NOTHING)

    def __str__(self):
        return self.nombre