from django.contrib import admin
from .models import ProgramaAcademico, UnidadAcademica

admin.site.register(ProgramaAcademico)
admin.site.register(UnidadAcademica)
