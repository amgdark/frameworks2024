from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.core.paginator import Paginator
from .models import UnidadAcademica, ProgramaAcademico
from .forms import FormUnidadAcademica, FormProgramaAcademico, FormFiltrosPrograma

def bienvenida(request):
    return render(request, 'bienvenida.html')
    

def editar_unidad(request, id):
    unidad = UnidadAcademica.objects.get(id=id)
    if request.method == 'POST':
        form = FormUnidadAcademica(request.POST, instance=unidad)
        if form.is_valid():
            form.save()
            return redirect('lista_unidades')
    else: 
        form = FormUnidadAcademica(instance=unidad)
    context = {'form': form }
    return render(request, 'editar_unidad.html', context)

def nuevo_programa(request): 
    form_unidad = FormUnidadAcademica()
    if request.method == 'POST':
        form = FormProgramaAcademico(request.POST)
        if form.is_valid():
            form.save()
            return redirect('lista_unidades')

    else:
        form = FormProgramaAcademico()
    
    context = {'form': form, 'form_unidad': form_unidad }
    return render(request, 'nuevo_programa.html', context)

def nueva_unidad_ajax(request):
    if request.method == 'POST':
        form = FormUnidadAcademica(request.POST)
        if form.is_valid():
            unidad = form.save()
            unidades = list(UnidadAcademica.objects.all().values())
            response = {
                'ok':'Se guardó la unidad con éxito',
                'unidades': unidades,
                'unidad': unidad.id
            }
            return JsonResponse(response, safe=False)
        else:
            return JsonResponse({'error': f"Error de validación: {form.errors}"}, safe=False)
    else:
        return JsonResponse({'error':'Método no permitido'}, safe=False)

   
def nueva_unidad(request):
    if request.method == 'POST':
        form = FormUnidadAcademica(request.POST)
        if form.is_valid():
            form.save()
            return redirect('lista_unidades')

    else:
        form = FormUnidadAcademica()
    
    context = {'form': form }
    return render(request, 'nueva_unidad.html', context)
    

def lista_unidades_academicas(request):
    context = {'unidades': UnidadAcademica.objects.all()}
    return render(request, 'lista_unidades.html', context)


def lista_programas(request):
    programas = ProgramaAcademico.objects.all()
    # programas = ProgramaAcademico.objects.get(id=2)
    # programas = ProgramaAcademico.objects.filter(nombre='Ingeniería de Software')
    # programas = ProgramaAcademico.objects.filter(nombre__startswith='I')
    # programas = ProgramaAcademico.objects.filter(nombre__endswith='ware')
    # programas = ProgramaAcademico.objects.filter(nombre__icontains='soft')
    # programas = ProgramaAcademico.objects.filter(nombre__iexact='soft')

    # programas = ProgramaAcademico.objects.exclude(nombre__startswith='I')[:2]
    # programas = ProgramaAcademico.objects.exclude(nombre__startswith='I')[2:4]
    
    print(programas.query)
    
    paginator = Paginator(programas, 2)
    page_number = request.GET.get("page")
    programas = paginator.get_page(page_number)

    if request.method == 'POST':
        form = FormFiltrosPrograma(request.POST)
        
        nombre = request.POST.get('nombre', None)
        descripcion = request.POST.get('descripcion', None)
        unidad = request.POST.get('unidad', None)
        
        if nombre:
            programas = programas.filter(nombre=nombre)
        
        if descripcion:
            programas = programas.filter(descripcion__icontains=descripcion)
                
        if unidad:
            programas = programas.filter(unidad_academica__nombre=unidad)

    else:
        form = FormFiltrosPrograma()
    context = {'programas': programas, 'form':form}
    return render(request, 'lista_programas.html', context)

def eliminar_unidad(request, id):
    unidad = UnidadAcademica.objects.get(id=id)
    programas = ProgramaAcademico.objects.filter(unidad_academica=unidad)
    print(programas)
    for programa in programas:
        programa.delete()
        
    unidad.delete()
    return redirect('lista_unidades')