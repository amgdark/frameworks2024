from django import forms
from .models import Institutcion

class FormInstiuciones(forms.ModelForm):
    
    class Meta:
        model = Institutcion
        fields = '__all__'