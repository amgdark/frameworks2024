from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from .models import Institutcion
from .forms import FormInstiuciones

class ListaInstituciones(ListView):
    model = Institutcion
    # template_name = ''


class NuevaInstitucion(CreateView):
    model = Institutcion
    # fields = '__all__'
    form_class = FormInstiuciones
    extra_context = {'accion':'Nueva'}
    success_url = reverse_lazy('lista_instituciones')
    
class EditarInstitucion(UpdateView):
    model = Institutcion
    # fields = '__all__'
    extra_context = {'accion':'Editar'}
    form_class = FormInstiuciones
    success_url = reverse_lazy('lista_instituciones')
    
class EliminarInstitucion(DeleteView):
    model = Institutcion
    success_url = reverse_lazy('lista_instituciones')
    
    